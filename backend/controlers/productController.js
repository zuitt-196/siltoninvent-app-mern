 const asyncHandler = require('express-async-handler');
const Product = require('../models/productModel');
const { fileSizeFormatter } = require('../utils/fileUpload');
const cloudinary = require('cloudinary').v2



// CREATE PRODUCT CONTROLLER FUNCTION 
const creteProduct  = asyncHandler (async (req, res) =>{
 const {name, sku, category, quantity, price,desc } = req.body;

//  co



    // validation if the fiels is not exist 
    if (!name || !sku || !category || !quantity || !price || !desc) {
        res.status(400)
        throw new Error("Please fill all fields")      
    }


    // Handle/Manage Image upload which save into cluoddimary 
    let fileData = {}
    if (req.file) {
        // save file/image to cloundinary
     let uploadedFile;
     try {
        uploadedFile = await cloudinary.uploader.upload(req.file.path, {folder: "siltoninvent-app", resource_type:"image"}) 
     } catch (error) {
        res.status(500)
        throw new Error("Image could not be uploaded")
     }

        fileData= {
            filename: req.file.originalname,
            filePath: uploadedFile.secure_url,
            fileName: req.file.mimetype,
            fileSize: fileSizeFormatter(req.file.size, 2)

        }
        
    }

    //Create product 
    const product = await Product.create({
        user: req.user.id,
        name,
        sku,
        category,
        quantity,
        price,
        desc,
        image:fileData
    })

    // console.log(product); and respons frontend
    res.status(201).json(product)
})


//  GET ALL PRODUCT CONTROLLER FUNCTION
// Retrive or Get all Products with the specific user ID that might be log in 
const getProducts = asyncHandler(async (req, res) => {
     const products = await Product.find({user: req.user.id}).sort("-createdAt")
    //  console.log(products);
    res.status(200).json(products)
    // console.log(req.user.email);
})






//GET SINGLE PRODUCT CONTROLLER FUNCTION
//Get single product
const getProductSingle = asyncHandler(async (req, res) => {
    const product = await Product.findById(req.params.id)
    // console.log(product.user.toString());
    // console.log(req.user.id);

    // validdation if no product exits 
    if (!product) {
        res.status(404)
        throw new Error('Product not found')
    }

    // const x = product.user === req.user.id ? true : false
    // console.log(x);

    // Rememnber every product has user since they are ref user scheman model
    // Match the Product to its user id with the req.user.id (came from log in user)
    // used of strint equality
    if (product.user.toString() !== req.user.id){
        res.status(401)
        throw new Error('User not Authorized')
    }

    res.status(200).json(product)
})



//DELETE PRODUCT CONTROLLER FUNCTION
const deteteProduct  = asyncHandler (async(req,res) => {
    const product = await Product.findById(req.params.id)

    // validation if product it is not exist
    if (!product) {
        res.status(401)
        throw new Error('Product not found')
    }

    // if product exits, Match the Product to its user id with the req.user.id (came from log in user)
    //used of strint equality
    if (product.user.toString() !== req.user.id){
        res.status(401)
        throw new Error('User not Authorized')
}

    // remove or delete the product
    await product.remove()
    res.status(200).json({message:"Product deleted successfully"})
    

})


// UPDATE PRODUCT CONTROLLER  
const updateProduct = asyncHandler (async(req,res) => {
    const {name, category, quantity, price,desc } = req.body;

    const {id} = req.params

    // find the product came from req.params and this params came also from creating product which is get there ID
    const product = await Product.findById(id)
        console.log(product.image);

    // if the product doest not exist 
    if (!product) {
        res.status(404)
        throw new Error("Product not found")
        
    }

    // Matcht Product to its req.user.id (req.user.id came from the user is log in )
     if (product.user.toString() !== req.user.id){
        res.status(401)
        throw new Error('User not Authorized')
    }



    // validation if the fiels is not exist 
    if (!name || !category || !quantity || !price || !desc) {
        res.status(400)
        throw new Error("Please fill all fields")      
    }


    // Handle/Manage Image upload which save into cluoddimary 
    let fileData = {}
    if (req.file) {
        // save file/image to cloundinary
     let uploadedFile;
     try {
        uploadedFile = await cloudinary.uploader.upload(req.file.path, {folder: "siltoninvent-app", resource_type:"image"}) 
     } catch (error) {
        res.status(500)
        throw new Error("Image could not be uploaded")
     }
        fileData= {
            filename: req.file.originalname,
            filePath: uploadedFile.secure_url,
            fileName: req.file.mimetype,
            fileSize: fileSizeFormatter(req.file.size, 2)

        }
        
    }

    // console.log(Object.keys(fileData).length);
    


    
    // Update product used by findByIdAndUpdate
    const updateProduct = await Product.findByIdAndUpdate(
        {
        _id:id,
        },
        {
            name,
            category,
            quantity,
            price,
            desc,
            image: Object.keys(fileData).length === 0 ? product.image : fileData,
        },{

            new: true,
            runValidators: true
        }   
        
        )
   
        // console.log(updateProduct._id);
        // console.log(id);
    // console.log(product);
    res.status(201).json(updateProduct)   

})
module.exports = {
    creteProduct,
    getProducts,
    getProductSingle,
    deteteProduct,
    updateProduct
}