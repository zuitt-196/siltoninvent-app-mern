const mongoose = require('mongoose');
const bcrypt = require("bcryptjs");

// scehma 

const userSchema = mongoose.Schema(
    {
        name:{
            type: String,
            required: [true, "plase add a name"] 
        },
        email:{
            type: String,
            required: [true, "plase add a email"],
            unique: true,
            trim:true,
            match: [    /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/, "Please enter a valid email"]  

        },
        password: {  type: String,
            required: [true, "plase add a passwoord"],
            minLength: [6, "Password must be up to  6 characters"],
            // maxLength: [23, "Password must not be more than  23 characters"]
        },
        photo:{
            type: String,
            required: [true, "plase add a photo"],
            default: "https://scontent.fdvo2-1.fna.fbcdn.net/v/t39.30808-6/322572092_571665530976957_5019462618475599715_n.jpg?_nc_cat=104&ccb=1-7&_nc_sid=09cbfe&_nc_eui2=AeFnxnyH-3T9ATTDgOwZVNB49uTgHOqOsr_25OAc6o6yv3RuB_hP_EZ7G8XB6q2-Q47_NadSPf-B_mzWpZOAxX8Z&_nc_ohc=Iw6bqF5xOD8AX-M7AXn&_nc_ht=scontent.fdvo2-1.fna&oh=00_AfC8v0XdvucKf2PFhpC2-Ila3QK5baLVGEvVwiyK4-eUsw&oe=63CF7BD1",
        },
        phone:{
            type: String,
            default: "+639"
        },
        bio:{
            type: String,
            maxLength:[250, "Bio must not be more than  250 characters"],
            default: "bio"
        }
            
        
    },
    
    {
        timestamps: true,
    }
)


// Ecrpt password before saving to DB
userSchema.pre("save", async function(next){

    // Define if we have changing  the profile of user the password property  or field will not modified 
    if (!this.isModified("password")) {
            return next();        
    }


    //hash passrword
    const salt = await bcrypt.genSalt(10)
    const hashedPassword = await bcrypt.hash(this.password, salt);
    // console.log(hashedPassword);
    this.password = hashedPassword
    next()
 

})

const User = mongoose.model('User', userSchema);

module.exports = User