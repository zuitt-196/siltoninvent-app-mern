const express = require('express');
const {creteProduct,getProducts, getProductSingle,deteteProduct, updateProduct} = require('../controlers/productController')
const protect = require('../middleWare/authMiddleWare');
const { upload } = require('../utils/fileUpload');
const router = express.Router();




// CREATE PRODUCT ROUTE 
router.post('/', protect, upload.single("image"), creteProduct);
//UPFDATE PRODUCT ROUTE
router.patch("/:id", protect, upload.single("image"), updateProduct) 
// GET/RETRIVE PRODUCTS ROUTE
router.get('/', protect, getProducts);
//GET/RETRIVE THE SINGLE PRODUCT ROUTE 
router.get('/:id', protect, getProductSingle);
// DELETE PRODUCT ROUTE 
router.delete("/:id", protect ,deteteProduct);




module.exports  = router;