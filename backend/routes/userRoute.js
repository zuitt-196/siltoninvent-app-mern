const express = require('express');
const { registerUser,loginUser, logOutUser,getUser,loginStatus,updateUser,changePassword,forgotpassword,resetPassword} = require('../controlers/userController');
const protect = require('../middleWare/authMiddleWare');
const router = express.Router();





// REGISTER USER ROUTE  SECTION
router.post('/register', registerUser);
// LOGIN USER ROUTE SECTION 
router.post('/login', loginUser);
//lOGOUT USER ROUTE SECTION 
router.get("/logout", logOutUser);
//GET THE USER ROUTE SECTION WITH MIDDLEWARE protect
router.get("/getuser", protect, getUser)
// GET USER LOGGEDIN STATUS SECTION 
router.get('/loggedin', loginStatus)
//  UTDATE THE USER SECTION
router.patch('/updateuser',protect, updateUser);
// UPDATE OR CHNAGE ROUTE SECTION 
router.patch('/changepassword', protect, changePassword);
//FORGOT PASSWORD ROUTE SECTION
router.post('/forgotpassword', forgotpassword);
//RESET PASSWORD ROUTE SECTION 
router.put("/resetpassword/:resetToken", resetPassword);





module.exports  = router;