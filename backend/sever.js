// IMPORT ALL DEPEDENECIES LIBRAYA TO  USE APPLICATION
const dotenv = require('dotenv').config()

const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const cors = require('cors');
const errorHandler = require('./middleWare/errorMIdldleWare');
const cookiesParser = require("cookie-parser")
const path = require('path');

// IMPORT ROUTES
const userRoutes = require('./routes/userRoute')
const productRoute = require('./routes/productRoute')
const contactRoute = require('./routes/contactRoute')

mongoose.set('strictQuery',false);



const app = express();


const PORT = process.env.PORT || 5000;




// MIDDLWWARE FUNCTION FOR RAW REQUSRTED  DATA FROM FRONT END SECTION 
app.use(express.json())
app.use(cookiesParser())
app.use(express.urlencoded({extended: false}))
app.use(bodyParser.json())
app.use(cors({
    origin: ["http://localhost:3000"],
    credentials:true
}));

// app.use(cors());

//Upload MIDDLEWARE SECTION 
app.use("/uploads", express.static(path.join(__dirname, "uploads")))

// ROUTE MIDDLEWARE SECTION 
app.use("/api/users",userRoutes)
app.use("/api/products", productRoute)
app.use("/api/contactus", contactRoute)




// ROUTE SECTION FOR HOMEPAGE AS DEFAULT
app.get("/" ,() => {
    res.send("home page")
})

// ERROR MIDLEWARE
app.use(errorHandler);



// Connect it DB and start server
mongoose.connect(process.env.MONGO_URI).then(() =>{
    app.listen(PORT, () => {
            console.log(`server Running on port ${PORT} na lods!!!!!`);
    })
}).catch((err) => {
    console.log({err});
})