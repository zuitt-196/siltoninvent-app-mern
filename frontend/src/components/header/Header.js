import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import{Link, useNavigate} from "react-router-dom"
import { SET_LOGIN, selecteName } from '../../redux/features/auth/authSlice'
import { logOutUser } from '../../services/authServices';

const Header = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const name  = useSelector(selecteName)
  console.log(name);

  const logout = async() => {
    await logOutUser()
    await dispatch(SET_LOGIN(false))
    navigate('/login')

  }

  
  return (
    <div className="--pad header">
          <div className="--flex-between">
              <h3>
                <span className="--fw-thin">Welcome, </span>
                <span className="--color-danger">{name} </span>
              </h3>
             <Link to="/login"> <button  className="--btn --btn-danger" onClick={logout}>Logout</button></Link>
          </div>
          <hr />

  </div>
)
}

export default Header
