import React from 'react'
import loaderImage from "../../assets/loader.gif"

import "./loader.scss"
// creete new portal or react populate in index.html 
import ReactDOM from 'react-dom'


const Loader = () => {
  return (
    ReactDOM.createPortal(
        <div className="wrapper">
                <div className="loader">
                        <img src={loaderImage} alt="loading" />
                </div>
        </div>, 
            document.getElementById("loader")
    )
  )
}

export const SpinnerImage = () => {
    return (
        <div className="--center-all">
                  <img src={loaderImage} alt="loading" />
        </div>
    )
}

export default Loader