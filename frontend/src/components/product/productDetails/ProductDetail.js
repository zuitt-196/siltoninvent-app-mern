import React,{ useEffect}from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useParams } from 'react-router-dom'
import useRedirectLoogeOutUse from '../../../customHook/useRedirectLoogeOutUse'
import { selectisLoggedIn } from '../../../redux/features/auth/authSlice'
import { getProductByid } from '../../../redux/features/product/productSlice'
import Card from '../../card/Card'
import { SpinnerImage } from '../../loader/Loader'
import './ProductDetail.scss'
import DOMPurify from 'dompurify';

const ProductDetail = () => {


    useRedirectLoogeOutUse("/login")
    
    const dispatch = useDispatch();

    const {id} = useParams()
    console.log(id);

    const isLoggdIn = useSelector(selectisLoggedIn)
    // definf the list of property came from redux statesdsadsad
    const  { product, isError, isLoading ,message} = useSelector((state) => state.product);
    // dispatch(getProductByid(id))
    // console.log("singleProduct:",product)

    //define stockStatus of product quantity 
    const stockStatus = (quantity) => {
        if (quantity > 0 ) {
            return <span className="--color-success">In Stock</span>
        }else{
          return <span className="--color-danger">Out of Stack</span>
        }
    }

    useEffect(() => {
      if (isLoggdIn === true) {
        dispatch(getProductByid(id));
      }
  
      if (isError) {
        console.log(message);
      }
    }, [isLoggdIn , isError, message, dispatch]);
  
  

  return (
    <div className='product-detail'>
        <h3 className='--mt'>Product Detail</h3>
    
        <Card cardClass={"card"}>
          {isLoading && <SpinnerImage/>}
          {product && (
              <div className="detail">
                  <Card cardClass="group">
                    {product?.image ? 
                      (<img src={product.image.filePath} alt={product.image.filePath} /> )
                    : <p>No image set for this product</p>}
                  </Card>
                <h4>Prdoct Availabilty: {stockStatus(product.quantity)} </h4>
                <hr />
                <h4><span className='badge'>Name:</span> &nbsp; {product.name}</h4>
                    <p>
                        <b>&rarr; SKU:</b> {product.sku}
                   </p>  
                   <p>
                        <b>&rarr; Category:</b> {product.category}
                   </p> 
                   <p>
                        <b>&rarr; Price:</b> {"$"}{product.price}
                   </p>
                   
                   <p>
                        <b>&rarr; Quantity in Stock:</b> {product.quantity}
                   </p> 
                   <p>
                        <b>&rarr; Total Value in Stock:</b> {"$"}{product.price * product.quantity}
                   </p> 
                   <hr />
                      <div dangerouslySetInnerHTML={{
                        __html:DOMPurify.sanitize(product.desc)}}>

                   
                      </div>
                      <div>

                      <hr />
                          <code className='--color-dark'>Created on: {product.createdAt.toLocaleString("en-US") || ""}</code><br />
                              <code className='--color-dark'>last Update on: {product.updatedAt.toLocaleString("en-US") || ""}</code>
                      </div>
              </div> 
          )}
        </Card>

    </div>
  )
}

export default ProductDetail
