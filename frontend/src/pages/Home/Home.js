import React from 'react'
import {RiProductHuntLine} from 'react-icons/ri'
import { Link } from 'react-router-dom'
import heroImg from '../../assets/inv-img.png'
import { ShowOnlogin, ShowOnlogout} from '../../components/protect/HiddenLink'

//IMPOERT STYLE home.scss
import "./Home.scss"

const Home = () => {
  return (
    <div className='home'>
        {/* NAVIGATION SECTION */}
         <nav className='container --flex-between'>
            <div className="logo">
                <RiProductHuntLine size={35}/>
            </div>
            <ul className="home-links">
                <ShowOnlogout>
                <li><Link to="/register">Register</Link></li>
                </ShowOnlogout>

                <ShowOnlogout>
                <li>
        
                    <button className='--btn --btn-primary'>
                        <Link to="/login">Login</Link>
                    </button>
                 </li> 
                 </ShowOnlogout>


                    <ShowOnlogin>          
                     <li>
                    <button className='--btn --btn-primary'>
                        <Link to="/dashboard">Dashboard</Link>
                    </button>
                 </li>   
                 </ShowOnlogin>
            </ul>
         </nav>
   

    {/* HERO SECTION */}
        <div className="container hero">
                {/* CONTAINER TEXT */}
                <div className="hero-text">
                        <h2>Inventory {"&"} Stock Management Solution</h2>
                        <p>Inventpory system to control and manage products in the warehouse in real time and intergrated to make it easier to develop ypur business</p>

                        <div className="hero-buttons">
                              <Link><button className="--btn --btn-primary">Free Trial 1 Month</button></Link>
                        </div>

                        <div className='--flex-start'>
                            <NunmberText num="14k" text="Brand Owners"/>
                            <NunmberText num="23k" text="Active user"/>
                            <NunmberText num="500+" text="Partners"/>
                        </div>        
                        
                     </div>

                {/* CONTAINER IMAGE */}
                <div className="hero-image">
                        <img src={heroImg} alt="" />
                </div>
        </div>
    </div>
  )
}

// creatre the regular function that contain on customize JSX wittargument of number and text
// furtther details must be know make sure this wraped the JSX {} cyrlyr braces
const NunmberText = ({num, text}) => {
     return (
        <div className="--mr">
                    <h3 className="--color-white">{num}</h3>
                    <p className="--color-white">{text}</p>
        </div>

     )
}
 
export default Home