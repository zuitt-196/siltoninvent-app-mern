import React, { useState } from 'react'
import styles from "./auth.module.scss"
import {BiLogIn} from "react-icons/bi"
import Card from '../../components/card/Card'
import { Link, useNavigate } from 'react-router-dom'

import { toast } from 'react-toastify'
import { loginUser, validateEmail } from '../../services/authServices'
//IMPORT S DESPATCH FROM REAC-REDUX 
import {useDispatch} from "react-redux";
//IMPORT SLICE THAT CONTAIN OF REDUCER METHPO OR FUNCTION 
import {SET_LOGIN, SET_NAME } from '../../redux/features/auth/authSlice'
import Loader from '../../components/loader/Loader'


const initialState = {
  email: "",
  password: "",
}

const LogIn = () => {
    // defien the dispatch method 
    const dispatch = useDispatch();

    // define the navigiation 
    const navigate = useNavigate();
  
    // define the loading 
    const [isLoading, setIsloading] = useState(false);
    // define and initial the  default data in register componenst
    const [formData, setFormData] = useState(initialState);
  
    // defracture the formData 
    const {email, password} = formData;

// define  functiion that changes in input elemenent 
const handleInputChange = (e) => {
  const {name , value} = e.target
  setFormData({...formData, [name]:value});

}

// define thee login function 
const userLogin = async (e) => {
e.preventDefault()

  // validation if they are not exists
  if( !email || !password ) {
    return toast.error("All fields are required")
}

// Please enter a valid email
if(!validateEmail){
  return toast.error("Please Enter a valid email");
}


// define and get the user data if login 
const userData = {
   email,
  password
}
setIsloading(true)



try {
  const loginData = await loginUser(userData)
      // console.log(loginData);
          await dispatch(SET_LOGIN(true))
         await dispatch(SET_NAME(loginData.name))
          navigate("/dashboard");
          setIsloading(false) ;
} catch (error) {
  setIsloading(false)   
}

}


  return (
    <div className={`container ${styles.auth}`}>
        {isLoading && <Loader/>}
    <Card>   
      <div className={styles.form}>
          <div className="--flex-center">
                  <BiLogIn size={35}/>
          </div>
          <h2>Log in</h2>


          {/* FORM SECTION */}
          <form onSubmit={userLogin}>
          <input type="email" 
            placeholder='Email..'
            required
            name='email'
            value={email}
            onChange={handleInputChange}
          />
          <input type="password" 
            placeholder='Password..'
            required
            name='password'
            value={password}
            autoComplete="on"
            onChange={handleInputChange}
          />
          <button type='submit' className='--btn --btn-primary --btn-block'>Log in</button>

          </form>
        <Link to= "/forgotpassword">Forgot Password</Link>
          <span className={styles.register}>
          <Link to="/">Home</Link>
            <p>&nbsp; Don't have an account &nbsp;   </p>
          <Link to="/register">Register</Link>

          </span>
     </div>
        </Card>
    </div>
  )
}

export default LogIn
