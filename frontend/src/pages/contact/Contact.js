import React,{useState} from 'react'
import Card from '../../components/card/Card';
import './Contact.scss';

//IMPORT ICONS 
import {FaPhoneAlt ,FaEnvelope, FaTwitter} from  'react-icons/fa'
import {TiLocationOutline} from "react-icons/ti"
import axios from 'axios';
import { toast } from 'react-toastify';
import { BACK_END } from '../../services/authServices';
 

const Contact = () => {
  // deifine the state 
  const [subject, setSubject] = useState("")
  const [message, setMessage] = useState("")

  const data = {
      subject,
      message,
  }

  // console.log("date:", data.subject);


  const sendEmail = async (e) => {
  e.preventDefault();

    // define to req or send in backend
  
    try {
      
        const response = await axios.post(`${BACK_END}/api/contactus`, data);
          console.log("response:", response.data);

        // after post the message make execute to reset of empty strinf the state
         setSubject("")
         setMessage("")
         toast.success(response.data.message)
    } catch (error) {
      toast.error(error.message)
      console.log("Error:", error.message);
    }

  }



  return (
    <div className="contact">
        <h3 className="--mt">Contact Us</h3>
        <div className="section">

            <form onSubmit={sendEmail}>
                <Card cardClass="card">
                    <label>Subject</label>
                      <input type="text" name="subject" placeholder="Subject" value={subject} required onChange={(e) => setSubject(e.target.value)}/>
                      <label>Message</label>
                      <textarea cols="30" rows="10"  name="message" required value={message} onChange={(e) =>  setMessage(e.target.value)}> </textarea>

                      <button className='--btn --btn-primary'>Send Message</button>
                </Card>
              </form>

               <div className='details'>
                     <Card cardClass="card2">
                        <h3>Our Contact Information </h3>
                        <p>Fill the form or contacr us Via other channels listed below</p>

                        <div className='icons'>
                            <span>
                                  <FaPhoneAlt size={20}/>
                                  <p>+09102764396</p>
                            </span> 
                             <span>
                                  <FaEnvelope size={20}/>
                                  <p>Message me on email <b>VhongBercasio@gmail.com</b></p>
                            </span>

                              <span>
                                   <TiLocationOutline size={20}/> 
                                  <p>
                                      Bislig City Surigao del Sur 
                                  </p> 
                              </span>

                            <span>
                                  <FaTwitter size={20}/>
                                  <p>@Vhongbercasio</p>
                            </span>
                        </div>
                    </Card> 
                  
              </div> 
              
        </div>
    </div>
  )
}

export default Contact
