import React,{useState,useEffect} from 'react'
import { useSelector } from 'react-redux'
import {  useNavigate } from 'react-router-dom'
import { toast } from 'react-toastify'
import Card from '../../components/card/Card'
import ChangesPassword from '../../components/changePassword/ChangesPassword'
import  Loader from '../../components/loader/Loader'
import useRedirectLoogeOutUse from '../../customHook/useRedirectLoogeOutUse'
import { selectisUser } from '../../redux/features/auth/authSlice'
import { updateUserProfle } from '../../services/authServices'
import "./Profile.scss"




const EditProfile = () => {
    useRedirectLoogeOutUse("/login")
    const navigate = useNavigate()
    
    // define the loading
    const [isLoading, setIsLoading] = useState(false);
    //define the initialState of user
    const user = useSelector(selectisUser);
    const {email} = user;

    // define useEffect if we refresh the page with this components the data came from redux will be lost when 
        useEffect(() => {
            if (!email) {
                navigate("/profile")
            }
        }, [email,navigate])


    const initialState = {
        name:user?.name,
        email:user?.email,
        phone:user?.phone,
        bio:user?.bio,
        photo:user?.photo,

       
    }

    const [profile, setProfile] = useState(initialState);
    const [profileImage, setProfileImage] = useState("");


// define  functiion that changes in input elemenent
const handleInputChange = (e) => {
    const {name , value} = e.target
    setProfile({...profile, [name]:value});
  
  }
  

  // define the funtion image file 
  const handleImageChange = (e) => {
    setProfileImage(e.target.files[0])
  };

  const saveProfile = async (e) => {
    e.preventDefault();
    setIsLoading(true);

    // this Section is implement of save the image on  cludinary 
    try {
    // handle image Uplod to cludinary 
    let imageURL ;
    console.log(profileImage ? true : false);
    if (profileImage && profileImage.type === 'image/jpeg' || profileImage.type === 'image/png' || profileImage.type === 'image/jpg') {
            const image = new FormData()
            image.append("file", profileImage)
            image.append("cloud_name", "dufokvyxf")
            image.append("upload_preset", "ejsjrt3q")

            // console.log("image:", image.append("file", profileImage))
            
            // first save image to cludinary 
            const response = await fetch("https://api.cloudinary.com/v1_1/dufokvyxf/image/upload", 
                {
                    method: "post",
                    body: image
                }
            )
            const imageData = await response.json()
            imageURL = imageData.url.toString()
            // console.log("withToString:", imageURL)
            // console.log("withoutToString:", imageURL.toString());
            console.log("imageURL:", imageURL);
            toast.success("Image Uploaded")

    }
            
            // Save profile data 
                const formData = {
                        name:profile.name,
                        phone:profile.phone,
                        bio:profile.bio,
                        photo: profileImage ? imageURL : profile.photo

                }
                    // update and pass unnto mongodb or database
                const updateData= await updateUserProfle(formData)
                    console.log(updateData);
                    toast.success("User Update");
                    navigate("/profile");
                    setIsLoading(false)
                    
    // }

    
    } catch (error) {
        console.log(error);
        setIsLoading(false)
        toast.error(error.message)

    }
}






  return (
    <div className="profile --my2">
            {isLoading && <Loader/>}

            <Card cardClass={"card --flex-dir-column"}>
                                <span className='profile-photo'>
                                    <img src={user?.photo} alt="profilepic" />
                                </span>


                                <form className="--form-control --mo "onSubmit={saveProfile} >

                                       <span className='profile-data'>
                                    <p>
                                        <label>Name:</label>
                                       <input type="text" name='name' value={profile?.name} onChange={handleInputChange}/>  
                                
                                    </p>

                                    <p>
                                        <label>Email</label>
                                     <input type="email" name="email" value={profile?.email} onChange={handleInputChange} disabled/> 
                                
                                    </p>

                                    <p>
                                        <label>Phone:</label>
                                        <input type="number" name="phone" value={ profile?.phone} onChange={handleInputChange} />
 
                                    </p>

                                    <p>
                                        <label>Bio:</label>
                                        <textarea name="bio" value={profile?.bio} cols="30" row="10"  onChange={handleInputChange} ></textarea>
                            
                                    </p>

                                            <p>
                                                <label> photo</label>   
                                                <input type="file" name='image' onChange={handleImageChange} />
                                            </p>

                                    <div>
                                        
                                    <button type = "submit" className='--btn --btn-primary'>Edit Profile</button>
                                         
                                    </div>
                             </span>
                             </form>
                            
                           
                        </Card>
                        <br />
                        
                        <ChangesPassword/>
                

    </div>
  )

}

export default EditProfile
