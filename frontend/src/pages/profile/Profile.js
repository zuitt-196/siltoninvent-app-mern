import React ,{useState, useEffect} from 'react'
import { useDispatch, } from 'react-redux';
import { Link } from 'react-router-dom';
import Card from '../../components/card/Card';
import { SpinnerImage } from '../../components/loader/Loader';
import useRedirectLoogeOutUse from '../../customHook/useRedirectLoogeOutUse'
import { SET_NAME, SET_USER } from '../../redux/features/auth/authSlice';
import { getUserProfle } from '../../services/authServices';
import './Profile.scss';



const Profile = () => {
    useRedirectLoogeOutUse("/login")
    const dispatch = useDispatch()
  
// define a state
    const [profile, setProfile] = useState(null);
    const [isLoading, setIsLoading] = useState(false);

        

    useEffect(() => {
        setIsLoading(true)
        const getUserData = async () => {
                const data = await getUserProfle()
                
           
                // push the data to  profile state
              setProfile(data)
              setIsLoading(false)
              dispatch(SET_USER(data))
              dispatch(SET_NAME(data.name))
              
        }
        getUserData()
    }, [dispatch])

  return (
    <div className='profile --my2'>
            {isLoading && <SpinnerImage/>}
            <> 
                {!isLoading &&  profile === null ? (
                        <p>Something went wrong, please reload the page...</p>
                  ) : (
                        <Card cardClass={"card --flex-dir-column"}>
                                <span className='profile-photo'>
                                    <img src={profile?.photo} alt="profilepic" />
                                </span>
                             <span className='profile-data'>
                                    <p>
                                        
                                        <b>Name: {profile?.name}</b>
                                
                                    </p>
                                    <p>
                                        <b>Email: {profile?.email}</b>
                                
                                    </p>
                                    <p>
                                        <b>Phone: {profile?.phone}</b>
                                
                                    </p>
                                    <p>
                                        <b>Bio: {profile?.bio}</b>
                                
                                    </p>

                                    <div>
                                            <Link to="/edit-profile">
                                                    <button className='--btn --btn-primary'>Edit Profile</button>
                                            </Link>
                                    </div>
                             </span>

                        </Card>

                  )}
            
            </>
    </div>
  )
}

export default Profile