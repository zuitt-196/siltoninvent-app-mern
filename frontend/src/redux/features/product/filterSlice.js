import { createSlice } from '@reduxjs/toolkit'

const initialState = {
    filterProducts:[]
}

const filterSlice = createSlice({
  name: "filter",
  initialState,
  reducers: {
    // to filter functio reducer 
    FILTER_PRODUCT(state,action) {
        // destracture the payload
        const { allProduct, search} = action.payload;
        //use the filter to find and filter the data inside the array object that mathes alse on search came from input failed element used the includes() method 
        const tempProducts  = allProduct.filter((Product) => Product.name.toLowerCase().includes(search.toLowerCase()) 
        || Product.category.toLowerCase().includes(search.toLowerCase()));
        

        state.filterProducts = tempProducts ;
    }
  }

  
});



export const {FILTER_PRODUCT} = filterSlice.actions


// export the some state that made of reducer that has manage the data and able to use it as  function some components or anyware in applicaation 
//it use for useSeletor 
// to gice and access in any componenst
export const selectFilterProduct = (state) => {
    return state.filter.filterProducts;
     
  }

export default filterSlice.reducer