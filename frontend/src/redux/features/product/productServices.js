import axios from "axios"


 const BACKEND_URL= process.env.REACT_APP_BACKEND_URL;

const API_URL = `${BACKEND_URL}/api/products/`;

// CREATE NEW PRODUCT FUNCTION 
 const createNewProduct = async (formData) => {
        const respones = await axios.post(API_URL, formData);
        // console.log(respones);
        return respones.data
}

//GET ALL PRODUCTS 
 const getAllProduct = async () => {
        const respones = await axios.get(API_URL);
            console.log("alprpduct:", respones.data)
            return respones.data

 }

 // UPDATE PRODUCT BY ID
 const updatProduct  = async (id , formData) => {
    const respones = await axios.patch(`${API_URL}${id}`, formData);
        console.log("alprpduct:", respones.data)
        return respones.data

}








//DELETE A PRODUCT by id 
const deleteProducts = async (id) => {
    const respones = await axios.delete(API_URL + id)
        return respones.data

}

//GET   A PRODUCT by id 
const getProducts = async (id) => {
    const respones = await axios.get(API_URL + id)
        // console.log(respones.data)
        return respones.data 
}


  

const productService = {
    createNewProduct,
    getAllProduct,
    getProducts,
    deleteProducts,
    updatProduct
}

export default productService;