import { createSlice, createAsyncThunk} from '@reduxjs/toolkit'
import { toast } from 'react-toastify';
import productService from './productServices';




const initialState = {
    product:null,
    allProduct:[],
    isError:false,
    isSucess:false,
    isLoading:false,
    message: "", 
    totalStoreValue: 0,
    outOfStock:2,
    category:[]

}

// CREATE NEW PRODUCT
export const createProduct =  createAsyncThunk(
        "products/create",
        async (formData, thunkAPI) => {
            try {
                // ge the the functoin of product services 

                return await productService.createNewProduct(formData)
                
            } catch (error) {
                const message= ( error.response && error.response.data && error.response.message)  || error.message || error.toString();
                console.log(message);
                 return thunkAPI.rejectWithValue(message)
            }
        }

)

// GET ALL PRODUCTS 
export const getAllProducts =  createAsyncThunk(
    "products/getAll",
    async (_, thunkAPI) => {
        try {
            // ge the the functoin of product services 
            return await productService.getAllProduct();
        } catch (error) {
            const message= ( error.response && error.response.data && error.response.message)  || error.message || error.toString();
            console.log(':', message);
             return thunkAPI.rejectWithValue(message)
        }
    }

)


// DELETE PRODUCT BY ID 
export const deleteProduct =  createAsyncThunk(
    "products/delete",
    async (id, thunkAPI) => {
        try {
            // ge the the functoin of product services 
            return await productService.deleteProducts(id);
        } catch (error) {
            const message= ( error.response && error.response.data && error.response.message)  || error.message || error.toString();
            console.log(message);
             return thunkAPI.rejectWithValue(message)
        }
    }

)

// UOPDATE  PRODUCT BY ID 
export const updateProductById =  createAsyncThunk(
    "products /updateProduct",
    async ({id , formData}, thunkAPI) => {
        try {
            // ge the the functoin of product services 
            return await productService.updatProduct(id, formData);
        } catch (error) {
            const message= ( error.response && error.response.data && error.response.message)  || error.message || error.toString();
            console.log(message);
             return thunkAPI.rejectWithValue(message)
        }
    }

)


// GET PRODUCT BY ID 
    export const  getProductByid =  createAsyncThunk(
    "products/getProduct",
    async (id, thunkAPI) => {
        try {
            // ge the the functoin of product services 
            console.log(id)
            return await productService.getProducts(id);
        } catch (error) {
            const message= ( error.response && error.response.data && error.response.message)  || error.message || error.toString();
            console.log(message);
             return thunkAPI.rejectWithValue(message)
        }
    }

)
                

const productSlice = createSlice({
  name: "product",
  initialState,
  reducers: {
    // calculate reducer the value of product price and qunatity 
    // CALCULATE ACTION.PAYLOAD
        CACL_STORE_VALUE(state,action){
   
            const products = action.payload
            const array = []
        
            // pushing the array
            products.map((item) =>{
                    const {price, quantity} = item
                    const productValue = price * quantity
                    return array.push(productValue);
            })

            // after pushing the value we going total it 
            const totalValue = array.reduce((a,b) => {
                return a + b
            },0)

            state.totalStoreValue = totalValue 
            // console.log(state.totalStoreValue);
        
        },
        // outstock reducer 
       //     CACL_OUTOFSTOCK ACTION.PAYLOAD
    CACL_OUTOFSTOCK(state,action){
        const products = action.payload
        const array = []
    
        // pushing the array
        products.map((item) =>{
                const { quantity} = item
                return array.push(quantity);
        })

        let count = 0
        // console.log(array)
    array.forEach((number ) =>{

            if ( number === 0 || number === "0") {
                count += 1
            }
        })    
        state.outOfStock = count
    },


    CACL_CATEGORAY(state,action ) {
        const products = action.payload
        const array = []
    
        // pushing the array
        products.map((item) =>{
                const { category} = item
                return array.push(category);
        })    

        
        // const uniqueCategory = [array]
        const uniqueCategory = [...new Set(array)]
        state.category = uniqueCategory
// console.log(state.category = uniqueCategory)
    }
  },


  // used createAsyncThunk method as the method for it  THIS EXTRA REDUCER 
  extraReducers: (builder) => {
    builder
        // createProduct createAsyncThunk() 
    .addCase(createProduct.pending, (state) =>{
            state.isLoading = true
    })
    
    .addCase(createProduct.fulfilled, (state, action ) =>{
        state.isLoading = false;
        state.isSucess = true;
        state.isError = false
        console.log(action.payload);
        state.allProduct.push(action.payload);
        toast.success("Product added successfully");

    })

    .addCase(createProduct.rejected, (state,action) => {
        state.isLoading = false;
        state.isError = true;
        state.message = action.payload;
        toast.error(action.payload);
        // if you notice the value of some state has aaction.payload it means they have already property to provide on the backennd
 
    })
    



    ///  getAllProducts createAsyncThunk() 
    .addCase(getAllProducts.pending, (state) =>{
        state.isLoading = true
})
    .addCase(getAllProducts.fulfilled, (state, action ) =>{
    state.isLoading = false;
    state.isSucess = true;
    state.isError = false
    // console.log(action.payloadd);
    state.allProduct = action.payload

    // toast.success("Product added successfully");
})
    .addCase(getAllProducts.rejected, (state,action) => {
    state.isLoading = false;
    state.isError = true ;
    state.message = action.payload;
    toast.error(action.payload);
    // if you notice the value of some state has aaction.payload it means they have already property to provide on the backennd

})






   ///  deleteProducts createAsyncThunk() 
  .addCase(deleteProduct.pending, (state) =>{
    state.isLoading = true
})

.addCase(deleteProduct.fulfilled, (state, action ) =>{
state.isLoading = false;
state.isSucess = true;
state.isError = false
toast.success("Product deleted successfully");

})

.addCase(deleteProduct.rejected, (state,action) => {
state.isLoading = false;
state.isError = true ;
state.message = action.payload;
toast.error(action.payload);
// if you notice the value of some state has aaction.payload it means they have already property to provide on the backennd
})

.addCase(getProductByid.pending, (state) => {
    state.isLoading = true;
  })
  .addCase(getProductByid.fulfilled, (state, action) => {
    state.isLoading = false;
    state.isSucess = true;
    state.isError = false;
    state.product = action.payload;
  })
  .addCase(getProductByid.rejected, (state, action) => {
    state.isLoading = false;
    state.isError = true;
    state.message = action.payload;
    toast.error(action.payload);
  })



  //UPDATE PRODUCT STATE BI ID EXRTREREDUCER 
  .addCase(updateProductById.pending, (state) => {
    state.isLoading = true;
  })
  .addCase(updateProductById.fulfilled, (state, action) => {
    state.isLoading = false;
    state.isSucess = true;
    state.isError = false;
    toast.success("Product Update  Succesfully")
  })
  .addCase(updateProductById.rejected, (state, action) => {
    state.isLoading = false;
    state.isError = true;
    state.message = action.payload;
    toast.error(action.payload);
  })


  }
});








export const {CACL_STORE_VALUE , CACL_OUTOFSTOCK, CACL_CATEGORAY} = productSlice.actions





// export the some state that made of reducer that has manage the data and able to use it as  function some components or anyware in applicaation 
//it use for useSeletor

 // defiene the loading of product
export const selectIsLoading = (state) => {
    // console.log(state.product)
    return state.product.isLoading


     
  }

    // defin the total Store value
  export const selectTotalStoreValue = (state) => {
    // console.log(state.product)
    return state.product.totalStoreValue
     
  }

  // define the out of stock of product
  export const selectOutOfStock= (state) => {
    // console.log(state.product)
    return state.product.outOfStock;
  }


  //  define the select of product
  export const selectProduct = (state) => {
    return state.product.product;
    
  } 


        // define the selectCategory of product
  export const selecCategory= (state) => {
    // console.log(state.product)
    return state.product.category;
  }







export default productSlice.reducer