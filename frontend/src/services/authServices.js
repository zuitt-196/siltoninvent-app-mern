import axios from "axios"
import {toast} from "react-toastify"




export const BACK_END = process.env.REACT_APP_BACKEND_URL

// validateb the email make sure it exist
// export it since we going to used it some  components 
// the concept of is to define the "Please enter a valid email"
 export const validateEmail = (email) => {
    return email.match(/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)
 }

 // REGISTRATION FUNCTION 
 export const  resgiterUser = async (userData) => {
try {
    // withCredentials:true is the property and value that build  in axios library  in order to get data that save in front end from the backend
    // withCredentials is to save the cookies which and tells the browser is login 
    // cookies is store  example the user is login the broswer it is save the  fronend broswer 
    // withCredentials:fasle -> default 
    const response = await axios.post(`${BACK_END}/api/users/register`, userData, {withCredentials: true })
     if(response.statusText === "OK"){
        toast.success("User Register successfully")
       
    }
    return response.data
} catch (error) {
        const messgae = ( error.response && error.response.data && error.response.message)  || error.message || error.toString();
        toast.error(messgae)
    
}
    
}

// LOGIN FUNCTION 
export const  loginUser = async (userData) => {
    try {
        // withCredentials:true is the property and value that build  in axios library  in order to get data that save in front end from the backend
        // withCredentials is to save the cookies which and tells the browser is login 
        // cookies is store  example the user is login the broswer it is save the  fronend broswer 
        // withCredentials:fasle -> default 
        // we can able to remove the withCredentials since we put inside the App.js, when we use  Axios http requsest they are execute to handle the data request from fronr into backend
        const response = await axios.post(`${BACK_END}/api/users/login`, userData)
         if(response.statusText === "OK"){
            toast.success("User Login successfully",response.data.email)
           
        }
        return response.data
    } catch (error) {
            const message = ( error.response && error.response.data && error.response.message)  || error.message || error.toString();
            toast.error("mali imo password nimal ka" + message)
        
    }
        
    }
    

    //LOGOUT USER FUNCTION  
    
    export const  logOutUser = async (userData) => {
        try {
            // withCredentials:true is the property and value that build  in axios library  in order to get data that save in front end from the backend
            // withCredentials is to save the cookies which and tells the browser is login 
            // cookies is store  example the user is login the broswer it is save the  fronend broswer 
            // withCredentials:fasle -> default 
            // we can able to remove the withCredentials since we put inside the App.js, when we use  Axios http requsest they are execute to handle the data request from fronr into backend
            const response = await axios.get(`${BACK_END}/api/users/logout`, userData)
             if(response.statusText === "OK"){
                toast.success("User Logout successfully")
            }
            
        } catch (error) {
                const messgae = ( error.response && error.response.data && error.response.message)  || error.message || error.toString();
                toast.error(messgae)
            
        }
            
        }


        // FORGOT PASSWORD FUNCTION 
        export const  forgotpassword = async (userData) => {
            try {
                // withCredentials:true is the property and value that build  in axios library  in order to get data that save in front end from the backend
                // withCredentials is to save the cookies which and tells the browser is login 
                // cookies is store  example the user is login the broswer it is save the  fronend broswer 
                // withCredentials:fasle -> default 
                // we can able to remove the withCredentials since we put inside the App.js, when we use  Axios http requsest they are execute to handle the data request from fronr into backend
                const response = await axios.post(`${BACK_END}/api/users/forgotpassword`, userData)
                    // if you confuse about the messgae it is came from API url forgot password 
                    toast.success(response.data.message)
                
                
            } catch (error) {
                    const messgae = ( error.response && error.response.data && error.response.message)  || error.message || error.toString();
                    toast.error(messgae)
                
            }
                
            }

            
            // RESET FUNCTION 
            export const  resetPassword = async (userData, resetToken) => {
                try {
                    // withCredentials:true is the property and value that build  in axios library  in order to get data that save in front end from the backend
                    // withCredentials is to save the cookies which and tells the browser is login 
                    // cookies is store  example the user is login the broswer it is save the  fronend broswer 
                    // withCredentials:fasle -> default 
                    // we can able to remove the withCredentials since we put inside the App.js, when we use  Axios http requsest they are execute to handle the data request from fronr into backend
                    const response = await axios.put(`${BACK_END}/api/users/resetpassword/${resetToken}`, userData);
                        // if you confuse about the messgae it is came from API url forgot password 
                        return response.data
                        // toast.success(response.data.message)
                
                    
                } catch (error) {
                        const messgae = ( error.response && error.response.data && error.response.message)  || error.message || error.toString();
                        toast.error(messgae)
                    
                }
                    
                }
    
 
        // GET LOGIN STATUS 
        export const getLoginStatus = async () => {
            try {
                // withCredentials:true is the property and value that build  in axios library  in order to get data that save in front end from the backend
                // withCredentials is to save the cookies which and tells the browser is login 
                // cookies is store  example the user is login the broswer it is save the  fronend broswer 
                // withCredentials:fasle -> default 
                // we can able to remove the withCredentials since we put inside the App.js, when we use  Axios http requsest they are execute to handle the data request from fronr into backend
                const response = await axios.get(`${BACK_END}/api/users/loggedin`);
                    // if you confuse about the messgae it is came from API url forgot password 
    
                    return response.data
                    // toast.success(response.data.message)
            
                
            } catch (error) {
                    const messgae = ( error.response && error.response.data && error.response.message)  || error.message || error.toString();
                    toast.error(messgae)
                
            }
                
            }

            // GET USER PROFILE
            export const getUserProfle = async () => {
                try {
                    // withCredentials:true is the property and value that build  in axios library  in order to get data that save in front end from the backend
                    // withCredentials is to save the cookies which and tells the browser is login 
                    // cookies is store  example the user is login the broswer it is save the  fronend broswer 
                    // withCredentials:fasle -> default 
                    // we can able to remove the withCredentials since we put inside the App.js, when we use  Axios http requsest they are execute to handle the data request from fronr into backend
                    const response = await axios.get(`${BACK_END}/api/users/getuser`);
                        // if you confuse about the messgae it is came from API url forgot password 
                        return response.data 
                        // toast.success(response.data.message)
                
                    
                } catch (error) {
                        const messgae = ( error.response && error.response.data && error.response.message)  || error.message || error.toString();
                        toast.error(messgae)
                    
                }
                    
                }


                  // UPDATE USER PROFILE
            export const updateUserProfle = async (formData) => {
                try {
                    // withCredentials:true is the property and value that build  in axios library  in order to get data that save in front end from the backend
                    // withCredentials is to save the cookies which and tells the browser is login 
                    // cookies is store  example the user is login the broswer it is save the  fronend broswer 
                    // withCredentials:fasle -> default 
                    // we can able to remove the withCredentials since we put inside the App.js, when we use  Axios http requsest they are execute to handle the data request from fronr into backend
                    const response = await axios.patch(`${BACK_END}/api/users/updateuser`, formData);
                        // if you confuse about the messgae it is came from API url forgot password 
                        return response.data 
                        // toast.success(response.data.message)
                
                    
                } catch (error) {
                        const messgae = ( error.response && error.response.data && error.response.message)  || error.message || error.toString();
                        toast.error(messgae)
                    
                }
                    
                }


                    // UPDATE USER PASSWORD OR CHANGES PASSWORD 
            export const changePassword = async (formData) => {
                try {
                    // withCredentials:true is the property and value that build  in axios library  in order to get data that save in front end from the backend
                    // withCredentials is to save the cookies which and tells the browser is login 
                    // cookies is store  example the user is login the broswer it is save the  fronend broswer 
                    // withCredentials:fasle -> default 
                    // we can able to remove the withCredentials since we put inside the App.js, when we use  Axios http requsest they are execute to handle the data request from fronr into backend
                    const response = await axios.patch(`${BACK_END}/api/users/changepassword`, formData);
                        // if you confuse about the messgae it is came from API url forgot password 
                        console.log("data:",response.data);
                        return response.data 
                        // toast.success(response.data.message)
                
                    
                } catch (error) {
                        const messgae = ( error.response && error.response.data && error.response.message)  || error.message || error.toString();
                        toast.error(messgae)
                    
                }
                    
                }
